package com.example.ruyoutest.watchPictureActivity

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.example.ruyoutest.R
import com.squareup.picasso.Picasso
import java.io.File

class WatchPictureActivity : AppCompatActivity() {

    lateinit var watchPictureImageView: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_watch_picture)

        watchPictureImageView = findViewById(R.id.watch_picture_image_view)

        Picasso.get().load(File(intent.getStringExtra(intentPictureName)) ).into(watchPictureImageView)
    }

    companion object {
        private const val intentPictureName = "picture"
        fun startWatchPictureActivity(context: Context, filepath: String) {
            context.startActivity(
                Intent(context, WatchPictureActivity::class.java).putExtra(
                    intentPictureName, filepath
                )
            )
        }
    }
}