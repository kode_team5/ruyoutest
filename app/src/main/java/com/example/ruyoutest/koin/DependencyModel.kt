package com.example.ruyoutest.koin

import com.example.ruyoutest.activityDetail.DetailRepository
import com.example.ruyoutest.activityDetail.DetailViewModel
import com.example.ruyoutest.activityMain.*
import com.example.ruyoutest.data.ApiService
import com.example.ruyoutest.data.MainRepository
import com.example.ruyoutest.data.RetrofitProvider
import com.example.ruyoutest.sendImageDialog.SendImageDialog
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

object DependencyModel {
    val mainModule = module {
        single { BodyPartsAdapter() }
        viewModel { MainViewModel(get()) }
        single {
            RetrofitProvider.getApiRetrofitClient()?.create(ApiService::class.java)
        }
        factory { MainRepositoryImpl() as MainRepository}
        factory { MainInteractorImpl() as MainInteractor }


    }
    val detailModule = module {
        viewModel { DetailViewModel(get(), get()) }
        single { DetailRepository(get()) }
        single { SendImageDialog() }
    }
}