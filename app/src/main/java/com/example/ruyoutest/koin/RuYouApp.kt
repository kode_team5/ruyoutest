package com.example.ruyoutest.koin

import android.app.Application
import com.example.ruyoutest.koin.DependencyModel.detailModule
import com.example.ruyoutest.koin.DependencyModel.mainModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class RuYouApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@RuYouApp)
            modules(listOf(mainModule, detailModule))
        }
    }
}