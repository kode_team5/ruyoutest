package com.example.ruyoutest.activityDetail

import android.util.Log
import com.example.ruyoutest.data.ApiService
import com.example.ruyoutest.models.BodyPartsModel
import com.example.ruyoutest.models.BodyPartsResponseModel
import com.example.ruyoutest.models.SendDataResponseModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import java.io.File

class DetailRepository(private val api: ApiService) {

    private val tag = this.javaClass.name
    private val compositeDisposable = CompositeDisposable()
    private val serverFullResponse: BehaviorSubject<String> = BehaviorSubject.create()

    fun sendData(id: Int, file: String, map:HashMap<String,String>) {

        api.sendData(id, File(file), map)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { addDataInListBodyParts(it) },
                { onError(it) },
                { Log.d(tag, "onComplite") }).addTo(compositeDisposable)
    }

    private fun onError(t: Throwable) {
        Log.d(
            tag, """ responce error. 
            | stackTrace= ${t.stackTrace} 
            | localizedMessage = ${t.localizedMessage}
            | suppressed = ${t.suppressed}
            | cause = ${t.cause}
            | message = ${t.message}
            | printStackTrace = ${t.printStackTrace()}
        """
        )

        serverFullResponse.onNext("${t.message}")
    }

    private fun addDataInListBodyParts(it: SendDataResponseModel) {
        Log.d(
            tag, "status = ${it.status} , ${it.msg}"
        )
        serverFullResponse.onNext("${it.status} , ${it.msg}")
    }
    fun getFullServerResponse():BehaviorSubject<String>{
        return serverFullResponse
    }

    fun onCleared() {
        compositeDisposable.clear()
    }

}