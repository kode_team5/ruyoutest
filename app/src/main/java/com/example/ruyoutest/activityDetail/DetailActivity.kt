package com.example.ruyoutest.activityDetail

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import com.example.ruyoutest.CashClass
import com.example.ruyoutest.R
import com.example.ruyoutest.sendImageDialog.SendImageDialog
import com.example.ruyoutest.watchPictureActivity.WatchPictureActivity
import com.squareup.picasso.Picasso
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import java.io.File
import java.io.IOException


class DetailActivity : AppCompatActivity(),
    SendImageDialog.OnMakePhotoButtonClick,
    SendImageDialog.OnNoButtonClick,
    SendImageDialog.OnPhotoTextViewClick,
    SendImageDialog.OnSendPhotoButtonClick {

    private val detailViewModel: DetailViewModel by viewModel()
    private val myDialogFragment: SendImageDialog by inject()
    private lateinit var imageView: ImageView
    val REQUEST_IMAGE_CAPTURE = 1
    lateinit var currentPhotoPath: String
    private var photoURI: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        //  findViewById<TextView>(R.id.target_text_view).text = CashClass.bodyPartsModel.target
        findViewById<TextView>(R.id.response_text_view).text =
            CashClass.bodyPartsResponseModelString
        imageView = findViewById<ImageView>(R.id.image_image_view)

        Picasso.get().load(R.drawable.bitmap_png)
            .into(imageView)

        findViewById<Button>(R.id.take_a_photo_button).setOnClickListener {
            dispatchTakePictureIntent()

        }
        detailViewModel.getServerFullResponse().observe(this, Observer {
            findViewById<TextView>(R.id.response_text_view).text = it
        })

        myDialogFragment.setOnPhotoTextViewClickListener(this)
        myDialogFragment.setOnNoButtonClickListener(this)
        myDialogFragment.setOnMakePhotoButtonClickListener(this)
        myDialogFragment.setOnSendPhotoButtonClickListener(this)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {

                showDialog()
            }
        }
    }

    override fun onPhotoTextViewClick() {
        Log.d(this.localClassName, "onPhotoTextViewClick")
        WatchPictureActivity.startWatchPictureActivity(this, currentPhotoPath)
    }

    override fun onNoButtonClick() {
        Log.d(this.localClassName, "onNoButtonClick")

    }

    override fun onMakePhotoButtonClick() {
        Log.d(this.localClassName, "onMakePhotoButtonClick")
        dispatchTakePictureIntent()
    }

    override fun onSendPhotoButtonClick(map: HashMap<String, String>) {
        Log.d(this.localClassName, "onSendPhotoButtonClick")
        detailViewModel.sendData(1, currentPhotoPath, map)
        findViewById<TextView>(R.id.response_text_view).text = ""

    }

    @Throws(IOException::class)
    private fun createImageFile(): File {

        val timeStamp: String = "image_picture"
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        //  val storageDir:File? = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            currentPhotoPath = absolutePath
            Log.d(this.javaClass.name, "$currentPhotoPath")
        }
    }


    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {

            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                Toast.makeText(this, "Error!", Toast.LENGTH_SHORT).show()
            }
            if (photoFile != null) {
                photoURI = FileProvider.getUriForFile(
                    this,
                    "com.example.android.provider",
                    photoFile
                )

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    private fun setPic() {
        Log.v("MainActivity", "setPic() function started")


        // Get the dimensions of the View
        val targetW = imageView.width
        val targetH = imageView.height

        // Get the dimensions of the bitmap
        val bmOptions = BitmapFactory.Options()
        bmOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
        val photoW = bmOptions.outWidth
        val photoH = bmOptions.outHeight

        // Determine how much to scale down the image
        val scaleFactor = Math.min(photoW / targetW, photoH / targetH)

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPurgeable = true
        val bitmap = BitmapFactory.decodeFile(currentPhotoPath, bmOptions)
        //  imageView.setImageBitmap(bitmap)
    }

    private fun showDialog() {
        val manager = supportFragmentManager
        myDialogFragment.show(manager, "myDialog")
    }

    companion object {
        fun startDetailActivity(context: Context) {
            context.startActivity(Intent(context, DetailActivity::class.java))
        }
    }


}