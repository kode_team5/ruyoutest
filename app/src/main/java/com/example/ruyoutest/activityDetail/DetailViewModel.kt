package com.example.ruyoutest.activityDetail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy

class DetailViewModel(application: Application, private val detailRepository: DetailRepository) :
    AndroidViewModel(application) {
    private val compositeDisposable = CompositeDisposable()
    private val serverFullResponse = MutableLiveData<String>("")

    init {
        detailRepository.getFullServerResponse().subscribeBy(onNext = {
            serverFullResponse.value = it
        }).addTo(compositeDisposable)
    }

    fun sendData(id:Int, file:String, map:HashMap<String,String>) {
        detailRepository.sendData(id,file,map)
    }

    fun getServerFullResponse(): MutableLiveData<String>{
        return serverFullResponse
    }

    override fun onCleared() {
        detailRepository.onCleared()
        compositeDisposable.clear()
    }
}