package com.example.ruyoutest.activityMain

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.ruyoutest.R
import com.example.ruyoutest.models.BodyPartsModel
import com.example.ruyoutest.models.BodyPartsResponseModel

class BodyPartsAdapter : RecyclerView.Adapter<BodyPartsAdapter.ViewHolder>() {

    private var data = ArrayList<BodyPartsModel>()
    private lateinit var viewHolderClickListener: ViewHolderClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_view_body_part, parent, false), viewHolderClickListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(data[position])
    }

    override fun getItemCount(): Int {
        return data.size
    }

    interface ViewHolderClickListener {
        fun onViewHolderClick(item: BodyPartsModel)
    }

    fun addListInData(list: List<BodyPartsModel>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    fun setListener(viewHolderClickListener: ViewHolderClickListener) {
        this.viewHolderClickListener = viewHolderClickListener
    }

    class ViewHolder(view: View, viewHolderClickListener: ViewHolderClickListener) :
        RecyclerView.ViewHolder(view) {
        var idTextView: TextView = view.findViewById(R.id.id_text_view)
        var bodyPartTextView: TextView = view.findViewById(R.id.body_part_text_view)
        lateinit var bodyPartsModel: BodyPartsModel

        init {
            itemView.setOnClickListener {
                viewHolderClickListener.onViewHolderClick(bodyPartsModel)
            }
        }

        fun bind(bodyPartsModel: BodyPartsModel) {
            this.bodyPartsModel = bodyPartsModel
            idTextView.text = bodyPartsModel.id.toString()
            bodyPartTextView.text = bodyPartsModel.target
        }

    }
}