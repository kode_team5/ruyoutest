package com.example.ruyoutest.activityMain

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.ruyoutest.models.BodyPartsModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import org.koin.core.KoinComponent
import org.koin.core.get

class MainViewModel(application: Application) :
    AndroidViewModel(application), KoinComponent {

    private val mainInteractor:MainInteractor = get()
    private val listBodyParts = MutableLiveData<ArrayList<BodyPartsModel>>()
    private val errorMessage = MutableLiveData<String>()
    private val serverFullResponse = MutableLiveData<String>("")
    private val compositeDisposable = CompositeDisposable()

    init {
        mainInteractor.getListResultBodyParts().subscribeBy(onNext = {
            listBodyParts.value = it
        }).addTo(compositeDisposable)

        mainInteractor.getErrorMessage().subscribeBy(onNext = {
            errorMessage.value = it
        }).addTo(compositeDisposable)

        mainInteractor.getFullServerResponse().subscribeBy(onNext = {
           serverFullResponse.value = it
        }).addTo(compositeDisposable)
    }

    override fun onCleared() {
        mainInteractor.onCleared()
        compositeDisposable.clear()
    }

    fun getBodyParts() {
        mainInteractor.getBodyParts()
    }

    fun getListResultBodyParts(): MutableLiveData<ArrayList<BodyPartsModel>> {
        return listBodyParts
    }

    fun getErrorMessage(): MutableLiveData<String> {
        return errorMessage
    }
    fun getServerFullResponse(): MutableLiveData<String>{
        return serverFullResponse
    }
}