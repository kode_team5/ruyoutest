package com.example.ruyoutest.activityMain

import android.util.Log
import com.example.ruyoutest.data.ApiService
import com.example.ruyoutest.data.MainRepository
import com.example.ruyoutest.models.BodyPartsModel
import com.example.ruyoutest.models.BodyPartsResponseModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent
import org.koin.core.get


class MainRepositoryImpl():MainRepository,KoinComponent {
    private val api: ApiService = get()
    private val tag = this.javaClass.name
    private val compositeDisposable = CompositeDisposable()
    private val listBodyParts: BehaviorSubject<ArrayList<BodyPartsModel>> = BehaviorSubject.create()
    private val serverFullResponse: BehaviorSubject<String> = BehaviorSubject.create()
    private val errorMessage: BehaviorSubject<String> = BehaviorSubject.create()

    override fun getBodyParts() {
        api.getBodyParts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { addDataInListBodyParts(it) },
                { onError(it) },
                { Log.d(tag, "onComplite") }).addTo(compositeDisposable)
    }

    override fun getListResultBodyParts(): BehaviorSubject<ArrayList<BodyPartsModel>> {
        return listBodyParts
    }

    override fun getErrorMessage(): BehaviorSubject<String> {
        return errorMessage
    }

    override fun getFullServerResponse(): BehaviorSubject<String> {
        return serverFullResponse
    }

    override fun onCleared() {
        compositeDisposable.clear()
    }

    private fun onError(t: Throwable) {
        Log.d(
            tag, """ responce error. 
            | stackTrace= ${t.stackTrace} 
            | localizedMessage = ${t.localizedMessage}
            | suppressed = ${t.suppressed}
            | cause = ${t.cause}
            | message = ${t.message}
            | printStackTrace = ${t.printStackTrace()}
        """
        )

        t.message?.let { errorMessage.onNext(it) }
    }

    private fun addDataInListBodyParts(it: BodyPartsResponseModel) {
        Log.d(
            tag, "status = ${it.status}"
        )
        if (it.data.isNotEmpty()) {
            listBodyParts.onNext(it.data as ArrayList<BodyPartsModel>)
        }
        serverFullResponse.onNext(it.toString())
    }

}