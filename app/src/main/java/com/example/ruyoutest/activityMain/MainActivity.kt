package com.example.ruyoutest.activityMain

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ruyoutest.CashClass
import com.example.ruyoutest.R
import com.example.ruyoutest.activityDetail.DetailActivity
import com.example.ruyoutest.models.BodyPartsModel
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity(), BodyPartsAdapter.ViewHolderClickListener {

    private val adapter: BodyPartsAdapter by inject()
    private val mainViewModel: MainViewModel by viewModel()
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adapter.setListener(this)

        recyclerView = findViewById(R.id.body_parts_recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = adapter

        bindingView()

          getBodyParts()

        findViewById<Button>(R.id.update_button).setOnClickListener {
            getBodyParts()
        }
        //
        //  DetailActivity.startDetailActivity(this)
        //
    }

    override fun onViewHolderClick(item: BodyPartsModel) {
        Log.d(this.localClassName, " ${item.id}, ${item.target} ")
        CashClass.bodyPartsModel = item
        DetailActivity.startDetailActivity(this)
    }

    private fun getBodyParts() {
        findViewById<LinearLayout>(R.id.progress_bar_layout).visibility = View.VISIBLE
        recyclerView.visibility = View.INVISIBLE
        findViewById<LinearLayout>(R.id.error_layout).visibility = View.INVISIBLE
        mainViewModel.getBodyParts()

    }
    private fun bindingView(){
        mainViewModel.getListResultBodyParts().observe(this, Observer {
            adapter.addListInData(it)
            findViewById<LinearLayout>(R.id.progress_bar_layout).visibility = View.INVISIBLE
            recyclerView.visibility = View.VISIBLE
            findViewById<LinearLayout>(R.id.error_layout).visibility = View.INVISIBLE
        })
        mainViewModel.getErrorMessage().observe(this, Observer {
            findViewById<LinearLayout>(R.id.progress_bar_layout).visibility = View.INVISIBLE
            recyclerView.visibility = View.INVISIBLE
            findViewById<LinearLayout>(R.id.error_layout).visibility = View.VISIBLE

            findViewById<TextView>(R.id.error_message_text_view).text = it
            DetailActivity.startDetailActivity(this)
        })
        mainViewModel.getServerFullResponse().observe(this, Observer {
            CashClass.bodyPartsResponseModelString = it
        })
    }
}