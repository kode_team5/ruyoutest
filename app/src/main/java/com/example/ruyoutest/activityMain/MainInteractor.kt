package com.example.ruyoutest.activityMain

import com.example.ruyoutest.data.ApiService
import com.example.ruyoutest.data.MainRepository
import com.example.ruyoutest.models.BodyPartsModel
import io.reactivex.subjects.BehaviorSubject
import org.koin.core.KoinComponent
import org.koin.core.get
import org.koin.core.inject

interface MainInteractor {
    fun getBodyParts()
    fun getListResultBodyParts(): BehaviorSubject<ArrayList<BodyPartsModel>>
    fun getErrorMessage(): BehaviorSubject<String>
    fun getFullServerResponse(): BehaviorSubject<String>
    fun onCleared()
}
class MainInteractorImpl:MainInteractor, KoinComponent {

    private val repository: MainRepository by inject()
    override fun getBodyParts() {
        repository.getBodyParts()
    }

    override fun getListResultBodyParts(): BehaviorSubject<ArrayList<BodyPartsModel>> {
        return repository.getListResultBodyParts()
    }

    override fun getErrorMessage(): BehaviorSubject<String> {
        return repository.getErrorMessage()
    }

    override fun getFullServerResponse(): BehaviorSubject<String> {
        return repository.getFullServerResponse()
    }

    override fun onCleared() {
        repository.onCleared()
    }
}