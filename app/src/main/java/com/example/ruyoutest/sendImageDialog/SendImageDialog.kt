package com.example.ruyoutest.sendImageDialog


import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import com.example.ruyoutest.R


class SendImageDialog : DialogFragment() {

    private lateinit var onPhotoTextViewClickListener: OnPhotoTextViewClick
    private lateinit var onNoButtonClickListener: OnNoButtonClick
    private lateinit var onMakePhotoButtonClickListener: OnMakePhotoButtonClick
    private lateinit var onSendPhotoButtonClickListener: OnSendPhotoButtonClick

    private val map=HashMap<String,String>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        val inflater = activity!!.layoutInflater
        val view: View = inflater.inflate(R.layout.dialog_send_image_layout, null)
        builder.setView(view)
        buildDialogView(view)
        return builder.create()
    }



    private fun buildDialogView(view: View) {
        view.findViewById<TextView>(R.id.photo_dialog_text_view)?.setOnClickListener {
            onPhotoTextViewClickListener.onPhotoTextViewClick()
        }
        view.findViewById<Button>(R.id.no_dialog_button)?.setOnClickListener {
            dismiss()
        }
        view.findViewById<Button>(R.id.update_photo_dialog_button)?.setOnClickListener {
            dismiss()
            onMakePhotoButtonClickListener.onMakePhotoButtonClick()

        }
        view.findViewById<Button>(R.id.send_photo_dialog_button)?.setOnClickListener {
            map.set("name",view.findViewById<EditText>(R.id.name_dialog_text_view).text.toString())
            map.set("surname",view.findViewById<EditText>(R.id.surname_dialog_text_view).text.toString())
            map.set("patronymic",view.findViewById<EditText>(R.id.middlename_dialog_text_view).text.toString())
            dismiss()
            onSendPhotoButtonClickListener.onSendPhotoButtonClick(map)
        }
    }

    fun setOnPhotoTextViewClickListener(listener: OnPhotoTextViewClick) {
        this.onPhotoTextViewClickListener = listener
    }

    fun setOnNoButtonClickListener(listener: OnNoButtonClick) {
        this.onNoButtonClickListener = listener
    }

    fun setOnMakePhotoButtonClickListener(listener: OnMakePhotoButtonClick) {
        this.onMakePhotoButtonClickListener = listener
    }

    fun setOnSendPhotoButtonClickListener(listener: OnSendPhotoButtonClick) {
        this.onSendPhotoButtonClickListener = listener
    }

    interface OnPhotoTextViewClick {
        fun onPhotoTextViewClick()
    }

    interface OnNoButtonClick {
        fun onNoButtonClick()
    }

    interface OnMakePhotoButtonClick {
        fun onMakePhotoButtonClick()
    }

    interface OnSendPhotoButtonClick {
        fun onSendPhotoButtonClick(map:HashMap<String,String>)
    }

}