package com.example.ruyoutest.data

import android.graphics.Bitmap
import com.example.ruyoutest.models.BodyPartsResponseModel
import com.example.ruyoutest.models.SendDataResponseModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query
import java.io.File

interface ApiService {

    @GET("get.php?action=get_bodyParts")
    fun getBodyParts(): Observable<BodyPartsResponseModel>

    @POST("send.php?action=send_data&")
    fun sendData(@Query("id") id: Int,
                 @Query("image") image: File,
                 @Query("contact") contact: HashMap<String,String>):Observable<SendDataResponseModel>


/*
GET  https://test-job.pixli.app/get.php?action=get_bodyParts
POST https://test-job.pixli.app/send.php
, @Query("concat") concat:ArrayList<>
Параметры:
- action (обязательное), значение send_data
- id (обязательное), тип integer
- image (обязательное), файл изображения
- contact, массив со строковыми полями полями name, surname, patronymic

https://test-job.pixli.app/send.php?action=send_data&id=1&image=C:\Users\denis\Desktop\Новая%20папка\miramistin.PNG
*/

}