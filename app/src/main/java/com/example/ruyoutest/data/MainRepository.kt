package com.example.ruyoutest.data

import com.example.ruyoutest.models.BodyPartsModel
import io.reactivex.subjects.BehaviorSubject

interface MainRepository {
    fun getBodyParts()
    fun getListResultBodyParts(): BehaviorSubject<ArrayList<BodyPartsModel>>
    fun getErrorMessage(): BehaviorSubject<String>
    fun getFullServerResponse(): BehaviorSubject<String>
    fun onCleared()
}